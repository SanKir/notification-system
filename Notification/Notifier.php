<?php

declare(strict_types=1);

namespace App\Service\Notification;

use App\Model\Email;
use App\Model\Sms;
use App\Service\Email\Repository\EmailRepository;
use App\Service\Notification\Message\Email\NotificationEmailMessage;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\Recipient;
use Twig\Environment;

class Notifier
{
    private MessageBusInterface $bus;
    private EmailRepository $emailRepository;
    private Environment $environment;

    public function __construct(
        MessageBusInterface $bus,
        EmailRepository $emailRepository,
        Environment $environment
    ) {
        $this->bus = $bus;
        $this->emailRepository = $emailRepository;
        $this->environment = $environment;
    }

    public function sendEmail(string $to, int $event, array $context = [])
    {
        $emailTemplate = $this->emailRepository->getEmailByEvent($event);

        if (!$emailTemplate) {
            return;
        }

        $notification = (new Notification())
            ->subject($this->environment->createTemplate($emailTemplate->getSubject())->render($context))
            ->content($this->environment->createTemplate($emailTemplate->getMessage())->render($context));

        $recipient = new Recipient($to);

        $emailMessage = new NotificationEmailMessage($notification, $recipient, $emailTemplate);
        $this->bus->dispatch($emailMessage);
    }
}
