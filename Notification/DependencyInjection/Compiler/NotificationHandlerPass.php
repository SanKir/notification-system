<?php

declare(strict_types=1);

namespace App\Service\Notification\DependecyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class NotificationHandlerPass implements CompilerPassInterface
{
    use PriorityTaggedServiceTrait;

    public const TAG_NAME = 'app.notification.notification_handler';

    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition(self::TAG_NAME);

        if ($definition->hasTag(self::TAG_NAME)) {
            $definition->clearTag(self::TAG_NAME);
        }

        $notificationHandlers = $this->findAndSortTaggedServices(self::TAG_NAME, $container);
        $definition->setArgument('$notificationHandlers', $notificationHandlers);
    }
}
