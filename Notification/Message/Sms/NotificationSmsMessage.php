<?php

declare(strict_types=1);

namespace App\Service\Notification\Message\Sms;

use App\Service\Notification\Message\Common\AbstractNotificationMessage;

class NotificationSmsMessage extends AbstractNotificationMessage
{
}
