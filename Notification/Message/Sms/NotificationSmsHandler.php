<?php

declare(strict_types=1);

namespace App\Service\Notification\Message\Sms;

use App\Service\Notification\Message\Common\NotificationHandlerInterface;
use App\Service\Notification\Message\Common\NotificationMessageInterface;
use Symfony\Component\Notifier\Message\SentMessage;
use Symfony\Component\Notifier\Message\SmsMessage;
use Symfony\Component\Notifier\TexterInterface;

class NotificationSmsHandler implements NotificationHandlerInterface
{
    private TexterInterface $texter;

    public function __construct(TexterInterface $texter)
    {
        $this->texter = $texter;
    }

    public function doInvoke(NotificationMessageInterface $message): ?SentMessage
    {
        $recipient = $message->getRecipient();
        $notification = $message->getNotification();

        $smsMessage = new SmsMessage($recipient->getPhone(), $notification->getContent());

        return $this->texter->send($smsMessage);
    }

    public function support(NotificationMessageInterface $message): bool
    {
        return $message instanceof NotificationSmsMessage;
    }
}
