<?php

declare(strict_types=1);

namespace App\Service\Notification\Message\Common;

use App\Service\Notification\Common\NotificationTemplateInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\Recipient;

class AbstractNotificationMessage implements NotificationMessageInterface
{
    private Notification $notification;
    private Recipient $recipient;
    private NotificationTemplateInterface $notificationTemplate;
    private ParameterBag $additionalParameters;

    public function __construct(
        Notification $notification,
        Recipient $recipient,
        NotificationTemplateInterface $notificationTemplate,
        array $additionalParameters = []
    ) {
        $this->notification = $notification;
        $this->recipient = $recipient;
        $this->notificationTemplate = $notificationTemplate;
        $this->additionalParameters = new ParameterBag($additionalParameters);
    }

    /**
     * @return Notification
     */
    public function getNotification(): Notification
    {
        return $this->notification;
    }

    /**
     * @return Recipient
     */
    public function getRecipient(): Recipient
    {
        return $this->recipient;
    }

    /**
     * @return NotificationTemplateInterface
     */
    public function getNotificationTemplate(): NotificationTemplateInterface
    {
        return $this->notificationTemplate;
    }

    /**
     * @return ParameterBag
     */
    public function getAdditionalParameters(): ParameterBag
    {
        return $this->additionalParameters;
    }
}
