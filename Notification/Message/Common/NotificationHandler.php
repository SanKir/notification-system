<?php

declare(strict_types=1);

namespace App\Service\Notification\Message\Common;

use App\Service\Notification\Event\NotificationSentEvent;
use LogicException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Notifier\Message\SentMessage;

class NotificationHandler implements MessageHandlerInterface, NotificationHandlerInterface
{
    private EventDispatcherInterface $dispatcher;
    private array $notificationHandlers = [];

    public function __construct(EventDispatcherInterface $dispatcher, array $notificationHandlers)
    {
        $this->dispatcher = $dispatcher;
        $this->notificationHandlers = $notificationHandlers;
    }

    public function __invoke(NotificationMessageInterface $message)
    {
        $sentMessage = $this->getNotificationHandler($message)->doInvoke($message);

        $this->dispatcher->dispatch(
            new NotificationSentEvent($sentMessage, $message->getRecipient(), $message->getNotificationTemplate()),
            NotificationSentEvent::NAME
        );
    }

    protected function getNotificationHandler(NotificationMessageInterface $message)
    {
        foreach ($this->notificationHandlers as $notificationHandler) {
            if ($notificationHandler->support($message)) {
                return $notificationHandler;
            }
        }

        throw new LogicException('Unreachable type of message');
    }

    public function doInvoke(NotificationMessageInterface $message): ?SentMessage
    {
        return null;
    }

    public function support(NotificationMessageInterface $message): bool
    {
        return true;
    }
}
