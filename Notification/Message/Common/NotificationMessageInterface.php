<?php

declare(strict_types=1);

namespace App\Service\Notification\Message\Common;

use App\Service\Notification\Common\NotificationTemplateInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\Recipient;

interface NotificationMessageInterface
{
    public function getNotification(): Notification;

    public function getRecipient(): Recipient;

    public function getNotificationTemplate(): NotificationTemplateInterface;

    public function getAdditionalParameters(): ParameterBag;
}
