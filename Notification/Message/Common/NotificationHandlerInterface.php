<?php

declare(strict_types=1);

namespace App\Service\Notification\Message\Common;

use Symfony\Component\Notifier\Message\SentMessage;

interface NotificationHandlerInterface
{
    public function doInvoke(NotificationMessageInterface $message): ?SentMessage;

    public function support(NotificationMessageInterface $message): bool;
}
