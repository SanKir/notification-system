<?php

declare(strict_types=1);

namespace App\Service\Notification\Message\Email;

use App\Service\Notification\Message\Common\AbstractNotificationMessage;

class NotificationEmailMessage extends AbstractNotificationMessage
{
}
