<?php

declare(strict_types=1);

namespace App\Service\Notification\Message\Email;

use App\Service\Notification\Message\Common\NotificationHandlerInterface;
use App\Service\Notification\Message\Common\NotificationMessageInterface;
use Symfony\Component\Mailer\Transport\TransportInterface;
use Symfony\Component\Notifier\Message\EmailMessage;
use Symfony\Component\Notifier\Message\SentMessage;

class NotificationEmailHandler implements NotificationHandlerInterface
{
    private TransportInterface $transport;

    public function __construct(TransportInterface $transport)
    {
        $this->transport = $transport;
    }

    public function doInvoke(NotificationMessageInterface $message): ?SentMessage
    {
        $recipient = $message->getRecipient();
        $notification = $message->getNotification();
        $message = EmailMessage::fromNotification($notification, $recipient);

        $mailerSentMessage = $this->transport->send($message->getMessage());

        $sentMessage = new SentMessage($message, get_class($this->transport));
        $sentMessage->setMessageId($mailerSentMessage->getMessageId());

        return $sentMessage;
    }

    public function support(NotificationMessageInterface $message): bool
    {
        return $message instanceof NotificationEmailMessage;
    }
}
