<?php

declare(strict_types=1);

namespace App\Service\Notification\Event\EventSubscriber;

use App\Model\VerificationCodeConstant;
use App\Service\Email\Dictionary\EmailConstant;
use App\Service\Form\Event\FormEvents;
use App\Service\Form\Event\SaveFormEvent;
use App\Service\Notification\Notifier;
use App\Service\User\Event\UserCreatedEvent;
use App\Service\User\Event\UserPasswordRestoredEvent;
use Creonit\VerificationCodeBundle\Event\CodeEvent;
use Creonit\VerificationCodeBundle\Event\VerificationEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class NotificationSubscriber implements EventSubscriberInterface
{
    private Notifier $notifier;
    protected UrlGeneratorInterface $urlGenerator;

    public function __construct(Notifier $notifier, UrlGeneratorInterface $urlGenerator)
    {
        $this->notifier = $notifier;
        $this->urlGenerator = $urlGenerator;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            UserCreatedEvent::NAME => ['onUserCreated', 1008],
            UserPasswordRestoredEvent::NAME => 'onUserPasswordRestored',
            VerificationEvents::CREATE_CODE => 'onCreateCode',
            FormEvents::SAVE_FORM => 'onFormSaved',
        ];
    }

    public function onUserCreated(UserCreatedEvent $event)
    {
        $user = $event->getUser();

        $this->notifier->sendEmail($user->getEmail(), EmailConstant::EVENT_REGISTRATION, [
            'name' => $user->getFirstName(),
            'password' => $user->getPlainPassword(),
        ]);
    }

    public function onUserPasswordRestored(UserPasswordRestoredEvent $event)
    {
        $user = $event->getUser();

        $this->notifier->sendEmail($user->getEmail(), EmailConstant::EVENT_RESTORE_PASSWORD, [
            'name' => $user->getFirstName(),
            'password' => $user->getPlainPassword(),
        ]);
    }

    public function onCreateCode(CodeEvent $event)
    {
        $verificationCode = $event->getVerificationCode();
        $scope = $verificationCode->getScope();

        if ($scope === VerificationCodeConstant::SCOPE_EMAIL) {
            $this->notifier->sendEmail($verificationCode->getKey(), EmailConstant::EVENT_RESTORE_PASSWORD_REQUEST, [
                'url' => $this->urlGenerator->generate(
                    'restore_password', ['code' => $verificationCode->getCode()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ]);
        }
    }

    public function onFormSaved(SaveFormEvent $event)
    {
        $formResult = $event->getResult();
        $form = $event->getResult()->getForm();
        $notification_emails = $formResult->getNotificationEmails();

        foreach ($notification_emails as $notificationEmail) {
            $this->notifier->sendEmail($notificationEmail, EmailConstant::EVENT_FEEDBACK_FORM_SAVED, [
                'subject' => $form->getTitle(),
                'html_template' => $formResult->renderResult(),
            ]);
        }
    }
}
