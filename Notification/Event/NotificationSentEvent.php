<?php

declare(strict_types=1);

namespace App\Service\Notification\Event;

use App\Service\Notification\Common\NotificationTemplateInterface;
use Symfony\Component\Notifier\Message\SentMessage;
use Symfony\Component\Notifier\Recipient\Recipient;
use Symfony\Contracts\EventDispatcher\Event;

class NotificationSentEvent extends Event
{
    public const NAME = 'notification.event.sent';

    private SentMessage $sentMessage;
    private Recipient $recipient;
    private NotificationTemplateInterface $notificationTemplate;

    public function __construct(
        SentMessage $sentMessage,
        Recipient $recipient,
        NotificationTemplateInterface $notificationTemplate
    ) {
        $this->sentMessage = $sentMessage;
        $this->recipient = $recipient;
        $this->notificationTemplate = $notificationTemplate;
    }

    public function getSentMessage(): SentMessage
    {
        return $this->sentMessage;
    }

    public function getRecipient(): Recipient
    {
        return $this->recipient;
    }

    public function getNotificationTemplate(): NotificationTemplateInterface
    {
        return $this->notificationTemplate;
    }
}
