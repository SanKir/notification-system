<?php

namespace App\Service\Sms\PoneSms\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class PoneCallApi extends AbstractPoneApi
{
    protected HttpClientInterface $client;
    protected string $token;
    protected string $host;

    public function __construct(string $token, HttpClientInterface $client, string $host = 'golos.p1sms.ru')
    {
        parent::__construct($token, $client, $host);
    }

    /**
     * @param array $callInfo
     * @return ResponseInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function createCallRequest(array $callInfo): ResponseInterface
    {
        $this->shortLinkSubstitution($callInfo['ivrs'][0]['webhookParameters']);
        $callInfo['ivrs'][0]['webhookParameters'] = json_encode($callInfo['ivrs'][0]['webhookParameters']);
        $params = ['json' => $callInfo];

        return $this->request(Request::METHOD_POST, 'apiCalls/create', $params);
    }

    protected function shortLinkSubstitution(array &$callInfo): array
    {
        $callInfo['text'] = strip_tags($callInfo['text'] ?? '');
        preg_match('#(https?:\/\/)?([\w-]{1,32}\.[\w-]{1,32})[^\s@]*#', $callInfo['text'], $links);

        if (isset($links[0])) {
            $callInfo['text'] = str_replace($links[0], '#shorturl#', $callInfo['text']);
            $callInfo['link'] = $links[0];
        }

        return $callInfo;
    }
}
