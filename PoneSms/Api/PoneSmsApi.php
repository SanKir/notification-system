<?php

declare(strict_types=1);

namespace App\Service\Sms\PoneSms\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class PoneSmsApi extends AbstractPoneApi
{
    protected HttpClientInterface $client;
    protected string $token;
    protected string $host;

    public function __construct(string $token, HttpClientInterface $client, string $host = 'admin.p1sms.ru')
    {
        parent::__construct($token, $client, $host);
    }

    /**
     * @param array $smsList
     * @return ResponseInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function createSmsRequest(array $smsList): ResponseInterface
    {
        foreach ($smsList as $key => $sms) {
            $smsList[$key] = $this->shortLinkSubstitution($sms);
        }

        $params = ['json' => ['sms' => $smsList]];

        return $this->request(Request::METHOD_POST, 'apiSms/create', $params);
    }

    protected function shortLinkSubstitution(array &$sms): array
    {
        preg_match('#(https?:\/\/)?([\w-]{1,32}\.[\w-]{1,32})[^\s@]*#', strip_tags($sms['text']) ?? '', $links);

        if (isset($links[0])) {
            $sms['text'] = str_replace($links[0], '#shorturl#', $sms['text']);
            $sms['link'] = $links[0];
        }

        return $sms;
    }

    public function getToken(): string
    {
        return $this->token;
    }
}
