<?php

declare(strict_types=1);

namespace App\Service\Sms\PoneSms\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Notifier\Exception\TransportException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

abstract class AbstractPoneApi
{
    protected HttpClientInterface $client;
    protected string $token;
    protected string $host;

    public function __construct(string $token, HttpClientInterface $client, string $host = 'admin.p1sms.ru')
    {
        $this->token = $token;
        $this->client = $client;
        $this->host = $host;
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param array $options
     * @return ResponseInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function request(string $method, string $endpoint, array $options = []): ResponseInterface
    {
        $options[$method === Request::METHOD_GET ? 'query' : 'json']['apiKey'] = $this->token;

        $response = $this->client->request($method, $this->buildUrl($endpoint), $options);

        if (200 !== $response->getStatusCode()) {
            $error = $response->toArray(false);

            throw new TransportException(
                sprintf('Unable to send the SMS: %s.', $error['data']['message']),
                $response
            );
        }

        return $response;
    }

    protected function buildUrl(string $endpoint)
    {
        $url = join('/', [
            $this->host,
            $endpoint,
        ]);

        return preg_replace_callback('/^((http[s]?):\/\/)?/', function($match) {
            return $match[0] ?: 'https://';
        }, $url);
    }
}
