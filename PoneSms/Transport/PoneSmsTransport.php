<?php

declare(strict_types=1);

namespace App\Service\Sms\PoneSms\Transport;

use App\Service\Sms\PoneSms\Api\PoneSmsApi;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Notifier\Exception\TransportException;
use Symfony\Component\Notifier\Exception\UnsupportedMessageTypeException;
use Symfony\Component\Notifier\Message\MessageInterface;
use Symfony\Component\Notifier\Message\SentMessage;
use Symfony\Component\Notifier\Message\SmsMessage;
use Symfony\Component\Notifier\Transport\AbstractTransport;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class PoneSmsTransport extends AbstractTransport
{
    protected const HOST = 'admin.p1sms.ru';

    private PoneSmsApi $api;

    public function __construct(
        string $token,
        HttpClientInterface $client = null,
        EventDispatcherInterface $dispatcher = null
    ) {
        $this->api = new PoneSmsApi($token, $client, $this->getEndpoint());

        parent::__construct($client, $dispatcher);
    }

    /**
     * @param MessageInterface $message
     * @return SentMessage
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function doSend(MessageInterface $message): SentMessage
    {
        if (!$message instanceof SmsMessage) {
            throw new UnsupportedMessageTypeException(__CLASS__, SmsMessage::class, $message);
        }

        $response = $this->api->request(Request::METHOD_POST, 'apiSms/create', [
            'json' => [
                'sms' => [
                    [
                        'channel' => 'char',
                        'sender' => 'VIRTA',
                        'phone' => $message->getPhone(),
                        'text' => $message->getSubject(),
                    ]
                ]
            ]
        ]);

        try {
            $result = $response->toArray(false);
        } catch (TransportExceptionInterface $e) {
            throw new TransportException('Could not reach the remote PoneSms server.', $response, 0, $e);
        }

        foreach ($result['data'] as $msg) {
            if ($msg['status'] === 'error') {
                throw new TransportException(
                    sprintf('Unable to send the SMS: %s (code %s).', $msg['errorDescription'], $msg['status']),
                    $response
                );
            }
        }

        $success = $response->toArray(false);

        $sentMessage = new SentMessage($message, (string) $this);
        $sentMessage->setMessageId((string) $success['data'][0]['id']);

        return $sentMessage;
    }

    public function supports(MessageInterface $message): bool
    {
        return $message instanceof SmsMessage;
    }

    public function __toString(): string
    {
        return sprintf('pone://%s', $this->getEndpoint());
    }
}
