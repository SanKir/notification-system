<?php

declare(strict_types=1);

namespace App\Service\Email\Unisender\Transport;

use Symfony\Component\Mailer\Exception\UnsupportedSchemeException;
use Symfony\Component\Mailer\Transport\AbstractTransportFactory;
use Symfony\Component\Mailer\Transport\Dsn;
use Symfony\Component\Mailer\Transport\TransportInterface;

class UnisenderTransportFactory extends AbstractTransportFactory
{
    public function create(Dsn $dsn): TransportInterface
    {
        $scheme = $dsn->getScheme();
        $username = $this->getUser($dsn);
        $token = $this->getPassword($dsn);

        if ('unisender+api' === $scheme) {
            return (new UnisenderApiTransport($token, $this->client, $this->dispatcher, $this->logger));
        }

        if ('unisender+smtp' === $scheme || 'unisender' === $scheme) {
            return (new UnisenderSmtpTransport($username, $token, $this->dispatcher, $this->logger));
        }

        throw new UnsupportedSchemeException($dsn, 'unisender', $this->getSupportedSchemes());
    }

    protected function getSupportedSchemes(): array
    {
        return ['unisender', 'unisender+api', 'unisender+smtp'];
    }
}
