<?php

declare(strict_types=1);

namespace App\Service\Email\Unisender\Transport;

use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class UnisenderSmtpTransport extends EsmtpTransport
{
    public function __construct(
        string $username,
        string $token,
        EventDispatcherInterface $dispatcher = null,
        LoggerInterface $logger = null
    ) {
        parent::__construct('smtp.eu1.unione.io', 465, true, $dispatcher, $logger);
        $this->setUsername($username);
        $this->setPassword($token);
    }
}
