<?php

declare(strict_types=1);

namespace App\Service\Email\Unisender\Transport;

use App\Service\Email\Unisender\Api\UnisenderApi;
use App\Service\Email\Unisender\Dictionary\UnisenderConstant;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\SentMessage;
use Symfony\Component\Mailer\Transport\AbstractApiTransport;
use Symfony\Component\Mime\Email;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class UnisenderApiTransport extends AbstractApiTransport
{
    protected const BASE_URL = 'https://api.unisender.com/ru/api/';
    protected UnisenderApi $api;

    public function __construct(
        string $token,
        HttpClientInterface $client = null,
        EventDispatcherInterface $dispatcher = null,
        LoggerInterface $logger = null
    ) {
        parent::__construct($client, $dispatcher, $logger);
        $this->api = new UnisenderApi($token, $client, $logger, self::BASE_URL);
    }

    protected function doSendApi(SentMessage $sentMessage, Email $email, Envelope $envelope): ResponseInterface
    {
        $response = $this->api->request(
            Request::METHOD_GET,
            'sendEmail',
            $this->getEmailData($email, $envelope)
        );
        $result = $response->toArray(false);

        $sentMessage->setMessageId($result['result']['email_id']);

        return $response;
    }

    protected function getEmailData(Email $email, Envelope $envelope): array
    {
        return [
            'email'=> $this->getRecipients($email, $envelope)[0]->getAddress(),
            'sender_name'=> 'Имя',
            'sender_email' => $envelope->getSender()->getAddress(),
            'subject'=> $email->getSubject(),
            'body'=> $email->getTextBody(),
            'list_id' => UnisenderConstant::NEW_USER_LIST,
        ];
    }

    public function __toString(): string
    {
        return sprintf('unisender+api://%s', $this->getBaseUrl());
    }

    protected function getBaseUrl(): string
    {
        return self::BASE_URL . ($this->port ? ":{$this->port}" : '');
    }
}
