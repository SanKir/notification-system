<?php

declare(strict_types=1);

namespace App\Service\Email\Unisender;

use App\Service\Email\Checker\EmailCheckerInterface;
use App\Service\Email\Unisender\Api\UnisenderApi;

class EmailService implements EmailCheckerInterface
{
    private UnisenderApi $client;

    public function __construct(UnisenderApi $client)
    {
        $this->client = $client;
    }

    public function checkEmail(string $messageId): bool
    {
        $data = [
            'email_id' => $messageId,
        ];

        $response = $this->client->request('GET', 'checkEmail', $data);

        $result = $response->toArray(false);
        $valid = false;

        if (isset($result['result']['statuses'][0])) {
            $valid = strpos($result['result']['statuses'][0]['status'], 'err_') === false &&
                strpos($result['result']['statuses'][0]['status'], 'skip_') === false;
        }

        return $valid;
    }
}
