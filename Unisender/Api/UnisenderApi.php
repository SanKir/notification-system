<?php

declare(strict_types=1);

namespace App\Service\Email\Unisender\Api;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class UnisenderApi
{
    protected HttpClientInterface $client;
    protected LoggerInterface $logger;
    protected string $token;
    protected string $baseUrl;

    /**
     * @param string $token
     * @param HttpClientInterface $client
     * @param LoggerInterface|null $logger
     * @param string $baseUrl
     */
    public function __construct(
        string $token,
        HttpClientInterface $client,
        ?LoggerInterface $logger,
        string $baseUrl = 'https://api.unisender.com/ru/api/'
    ) {
        $this->token = $token;
        $this->client = $client;
        $this->logger = $logger ?? new NullLogger();
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param array $options
     * @return mixed
     */
    public function request(string $method, string $endpoint, array $options = []): ResponseInterface
    {
        try {
            $variables = ['format' => 'json', 'api_key' => $this->token];
            $variables += $options;

            if ($method === Request::METHOD_GET) {
                $url = sprintf('%s%s?%s', $this->baseUrl, $endpoint, http_build_query($variables));
                $variables = [];

            } else {
                $url = sprintf('%s%s', $this->baseUrl, $endpoint);
            }

            $response = $this->client->request($method, $url, $variables);

        } catch (ExceptionInterface $e) {
            $this->logger->error("UnisenderRequestError: {$e->getMessage()}", [
                'uri' => $endpoint,
            ]);
        }

        return $response;
    }
}
