<?php

declare(strict_types=1);

namespace App\Service\Email\Unisender;

use App\Model\User;
use App\Service\Email\Command\Subscriber\EmailSubscriberInterface;
use App\Service\Email\Unisender\Api\UnisenderApi;
use App\Service\Email\Unisender\Dictionary\UnisenderConstant;

class ContactService implements EmailSubscriberInterface
{
    private UnisenderApi $client;
    private int $listId;

    public function __construct(UnisenderApi $client)
    {
        $this->client = $client;
        $this->listId = UnisenderConstant::NEW_USER_LIST;
    }

    public function subscribe(User $user)
    {
        $data = [
            'list_ids' => $this->listId,
            'fields[email]' => $user->getEmail(),
            'fields[Name]' => $user->getFullName(),
            'double_optin' => 3,
        ];

        $response = $this->client->request('GET', 'subscribe', $data);

        return $response;
    }

    public function exclude(User $user)
    {
        $data = [
            'list_ids' => $this->listId,
            'contact_type' => 'email',
            'contact' => $user->getEmail(),
        ];

        $response = $this->client->request('GET', 'exclude', $data);

        return $response;
    }
}
