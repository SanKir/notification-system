# Notification system

This code example demonstrates the notification system.

`Unisender` is used as a mail transport.

`PoneSMS` is used as SMS transport.

## What was used?

Framework: Symfony

Components: Messenger Component, EventDispatcher Component 